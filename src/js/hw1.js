
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get salary() {
    return this._salary;
  }

  set name(name) {
    this._name = name;
  }

  set age(age) {
    this._age = age;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const employee = new Employee("Іван", "25", 2000);

const programmerOne = new Programmer("Сергій", "33", 2000);
const programmerTwo = new Programmer("Марічка", "19", 3000);
const programmerThree = new Programmer("Василь", "51", 3450);

console.log(employee.salary);
console.log(programmerOne.salary);
console.log(programmerTwo.salary);
console.log(programmerThree.salary);
