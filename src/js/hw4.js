const insertList = document.querySelector(".movie-list");
const url = `https://ajax.test-danit.com/api/swapi/films`;

function state(res) {
  if (!res.ok) {
    throw new Error("Network response was not ok");
  } else {
    return res.json();
  }
}

function renderName(data) {
  const list = data.map(({ episodeId, name, openingCrawl }) => {
    return `<li class='movie-name'><b>Episode</b>: ${episodeId} <br><b>Name</b>: ${name} <br><b>Crawl</b>: ${openingCrawl}</li><img class='spinner' src="./images/bars.svg" />`;
  });
  insertList.insertAdjacentHTML("afterbegin", list.join(""));
}

function renderCharacters(data, i) {
  const spinners = document.querySelectorAll(".spinner");
  const mivieItem = document.querySelectorAll(".movie-name");
  const elUl = document.createElement("ul");
  const p = document.createElement("p");

  setTimeout(() => {
    spinners[i].classList.add("no-show");
    p.innerHTML = "<b>Characters:</b>";
    const listChar = data.map(({ name }) => `<li>${name}</li>`);
    elUl.insertAdjacentHTML("afterbegin", listChar.join(""));
    mivieItem[i].append(p);
    mivieItem[i].append(elUl);
  }, 500);
}

fetch(url)
  .then((response) => state(response))
  .then((data) => {
    renderName(data);

    data.forEach((item, index) => {
      const promises = item.characters.map((i) =>
        fetch(i).then((response) => state(response))
      );
      Promise.all(promises).then((data) => {
        renderCharacters(data, index);
      });
    });
  })
  .catch((error) => {
    console.error("There was a problem with the fetch operation:", error);
  });
