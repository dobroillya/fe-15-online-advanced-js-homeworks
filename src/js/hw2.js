const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
let list = ``;

const listValid = books.forEach(book => {
  try {
    if(!book.name){
      throw new Error(`name is undefined`)
    }
    if(!book.author){
        throw new Error(`author is undefined`)
      }
    if(!book.price){
        throw new Error(`price is undefined`)
    }

    list += `<li>${book.author} "${book.name}", ціна: $${book.price}</li>`
  } catch (error) {
    console.log("An error occurred: " + error.message);
  }
});

const ul = document.createElement('ul');
ul.insertAdjacentHTML('afterbegin', list);
root.append(ul)

console.log(root);
