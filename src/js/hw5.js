const listOfCards = document.querySelector(".card-container");

const callApi = async function (url) {
  const response = await fetch(url);

  try {
    if (!response.ok) {
      throw new Error("Network response was not ok");
    } else {
      return response.json();
    }
  } catch (error) {
    console.log(error);
  }
};

class Card {
  constructor(id, title, text, name, email) {
    this.id = id;
    this.title = title;
    this.text = text;
    this.name = name;
    this.email = email;
  }
  generateHtml() {
    return `<div data-id=${this.id} class="bg-white shadow-lg rounded-lg overflow-hidden">
                    <div class="p-4">
                    <h2 class="text-lg font-bold mb-2">${this.title}</h2>
                    <p class="text-gray-700 text-base">${this.text}</p>
                    </div>
                    <div class="px-4 py-2 bg-gray-100 flex place-content-between">
                    <div>
                        <p class="text-gray-600 text-sm mb-1">${this.name}</p>
                        <p class="text-gray-600 text-sm">${this.email}</p>
                    </div>
                    <svg
                        class="delete-item"
                        width="30"
                        xmlns="http://www.w3.org/2000/svg"
                        shape-rendering="geometricPrecision"
                        text-rendering="geometricPrecision"
                        image-rendering="optimizeQuality"
                        fill-rule="evenodd"
                        clip-rule="evenodd"
                        viewBox="0 0 512 511.99"
                    >
                        <path
                        fill="#FC4032"
                        fill-rule="nonzero"
                        d="M256 0c70.68 0 134.69 28.66 181.01 74.98C483.35 121.31 512 185.31 512 255.99c0 70.68-28.66 134.69-74.99 181.02-46.32 46.32-110.33 74.98-181.01 74.98-70.68 0-134.69-28.66-181.02-74.98C28.66 390.68 0 326.67 0 255.99S28.65 121.31 74.99 74.98C121.31 28.66 185.32 0 256 0zm116.73 236.15v39.69c0 9.39-7.72 17.12-17.11 17.12H156.38c-9.39 0-17.11-7.7-17.11-17.12v-39.69c0-9.41 7.69-17.11 17.11-17.11h199.24c9.42 0 17.11 7.76 17.11 17.11zm37.32-134.21c-39.41-39.41-93.89-63.8-154.05-63.8-60.16 0-114.64 24.39-154.05 63.8-39.42 39.42-63.81 93.89-63.81 154.05 0 60.16 24.39 114.64 63.8 154.06 39.42 39.41 93.9 63.8 154.06 63.8s114.64-24.39 154.05-63.8c39.42-39.42 63.81-93.9 63.81-154.06s-24.39-114.63-63.81-154.05z"
                        />
                    </svg>
                    </div>
                </div>`;
  }
}

function getUser(dataUsers, userId) {
  return dataUsers.filter((i) => i.id === userId)[0];
}

callApi("https://ajax.test-danit.com/api/json/posts").then((dataPosts) => {
  callApi("https://ajax.test-danit.com/api/json/users").then((dataUsers) => {
    console.log(dataUsers);
    console.log(dataPosts);
    dataPosts.forEach(({ id, body, title, userId }) => {
      const { name, email } = getUser(dataUsers, userId);
      const card = new Card(id, title, body, name, email);
      listOfCards.insertAdjacentHTML("afterbegin", card.generateHtml());
    });
  });
});

listOfCards.addEventListener("click", (e) => {
  if (e.target.classList[0] === "delete-item") {
    const idPost = e.target.parentNode.parentNode.dataset.id;
    fetch(`https://ajax.test-danit.com/api/json/posts/${idPost}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.ok) {
          e.target.parentNode.parentNode.remove();
        } else {
          throw new Error("not ok");
        }
      })
      .catch((error) => console.log(error));
  }
});
