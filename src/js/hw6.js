const btn = document.querySelector(".btn");
const info = document.querySelector(".info");

async function getData(url) {
  const response = await fetch(url);
  if (response.status === 200) {
    return response.json();
  }
}

async function getIp() {
  info.innerHTML = '';
  try{
    const dataIp = await getData("https://api.ipify.org/?format=json");

  const yourData = await getData(
    `https://ipapi.co/${dataIp.ip}/json`
  );

  console.log(yourData);
  const { country_name, region, city } = yourData;
  info.insertAdjacentHTML(
    'afterbegin',
    `<p><b>Країна: </b>${country_name}</p> <p><b>Регіон: </b>${region}</p> <p><b>Місто: </b>${city} </p>`
  );

  }
  catch (error){
    console.log(error);
  }

}

btn.addEventListener("click", getIp);
